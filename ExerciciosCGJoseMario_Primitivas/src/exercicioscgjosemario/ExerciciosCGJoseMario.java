/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicioscgjosemario;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import javax.swing.JFrame;

/**
 *
 * @author Alunos
 */
public class ExerciciosCGJoseMario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GLProfile profile = GLProfile.get(GLProfile.GL2);
        
        GLCapabilities capabilities = new GLCapabilities(profile);
        
        GLCanvas canvas = new GLCanvas(capabilities);
        canvas.addGLEventListener(new Renderer());
        
        JFrame frame = new JFrame("Aplicação JOGL Simples");
        frame.getContentPane().add(canvas);
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    
}
